﻿using System.Collections.Generic;
using EnsureThat;

namespace OriflammeEngine.Role
{
    internal sealed class RoleSolver : IRoleSolver
    {
        private readonly IDictionary<CardIdentity, Role> _playerRoles = new Dictionary<CardIdentity, Role>();

        public RoleSolver(IEnumerable<RoleCard> cards)
        {
            foreach (var (id, role) in cards)
            {
                _playerRoles.Add(id, role);
            }
        }

        /// <inheritdoc cref="Get" />
        public Role Get(CardIdentity cardIdentity)
        {
            Ensure.That(_playerRoles.ContainsKey(cardIdentity)).IsTrue();

            return _playerRoles[cardIdentity];
        }

        /// <inheritdoc cref="Give" />
        public void Give(CardIdentity cardIdentity, Role role)
        {
            Ensure.That(_playerRoles.ContainsKey(cardIdentity)).IsTrue();

            _playerRoles[cardIdentity] = role;
        }
    }
}