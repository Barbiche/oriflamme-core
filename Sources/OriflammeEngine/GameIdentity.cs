﻿using System;
using Equ;

namespace OriflammeEngine
{
    /// <summary>
    ///     Represents the identity of a game.
    /// </summary>
    public sealed class GameIdentity : MemberwiseEquatable<GameIdentity>
    {
        private readonly Guid _guid = Guid.NewGuid();

        public static implicit operator Guid(GameIdentity id)
        {
            return id._guid;
        }
    }
}