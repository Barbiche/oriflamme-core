﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     A request to a player to handle the spy behavior (choose an adjacent card).
    /// </summary>
    public sealed class SpyCommand : IPlayerBetweenManyCommand
    {
        private readonly Action<PlayerIdentity> _callback;

        public SpyCommand(PlayerIdentity              waitedInputPlayer,
                          CardIdentity                spyCard,
                          IEnumerable<PlayerIdentity> availablePlayers,
                          Action<PlayerIdentity>      callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            SpyCard           = spyCard;
            AvailablePlayers  = availablePlayers.ToImmutableArray();
        }

        public CardIdentity                   SpyCard           { get; }
        public ImmutableArray<PlayerIdentity> AvailablePlayers  { get; }
        public PlayerIdentity                 WaitedInputPlayer { get; }

        public void SendResult(PlayerIdentity result)
        {
            _callback(result);
        }
    }
}