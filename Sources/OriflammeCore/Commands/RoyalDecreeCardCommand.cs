﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public sealed class RoyalDecreeCardCommand : ICardBetweenManyCommand
    {
        private readonly Action<CardIdentity> _callback;

        public RoyalDecreeCardCommand(PlayerIdentity            waitedInputPlayer,
                                      CardIdentity              royalDecreeCard,
                                      IEnumerable<CardIdentity> availableCards,
                                      Action<CardIdentity>      callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            RoyalDecreeCard   = royalDecreeCard;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public CardIdentity   RoyalDecreeCard   { get; }
        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(CardIdentity result)
        {
            _callback(result);
        }

        public ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}