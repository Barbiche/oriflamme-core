﻿using System.Collections.Generic;

namespace OriflammeEngine.PlayerInfluence
{
    /// <summary>
    ///     Represent a solver for the player influences.
    /// </summary>
    public interface IPlayerInfluenceSolver
    {
        /// <summary>
        ///     Get the score of a player.
        /// </summary>
        /// <param name="player">The player whom the score is wanted.</param>
        /// <returns>Score of the player.</returns>
        int Get(PlayerIdentity player);

        /// <summary>
        ///     Apply one or many score changes.
        /// </summary>
        /// <param name="influenceChanges">Score changes to apply.</param>
        void ApplyChanges(IEnumerable<InfluenceChange> influenceChanges);

        /// <summary>
        ///     Apply one score change.
        /// </summary>
        /// <param name="influenceChange">A change to apply.</param>
        void ApplyChanges(InfluenceChange influenceChange);

        /// <summary>
        ///     Give influence to a player.
        /// </summary>
        /// <param name="player">Player to give the influence.</param>
        /// <param name="influenceGain">Influence gained by the player.</param>
        /// <returns></returns>
        int Give(PlayerIdentity player, int influenceGain);
    }
}