﻿namespace OriflammeEngine.Stack
{
    public sealed class GameStackPointerFactory : IGameStackPointerFactory
    {
        private readonly ICardRemovedExposer _cardRemovedExposer;
        private readonly IGameStack          _gameStack;

        public GameStackPointerFactory(IGameStack gameStack, ICardRemovedExposer cardRemovedExposer)
        {
            _gameStack          = gameStack;
            _cardRemovedExposer = cardRemovedExposer;
        }

        public IGameStackPointer Create()
        {
            return new GameStackPointer(_gameStack, _cardRemovedExposer);
        }
    }
}