﻿using System;
using System.Collections.Generic;
using BarbFoundation;
using BarbFoundation.DataStructures;

namespace OriflammeEngine.Crown
{
    internal sealed class CrownSolver : ICrownSolver
    {
        private readonly CircularLinkedList<PlayerIdentity>     _players;
        private          CircularLinkedListNode<PlayerIdentity> _crownedPlayer;

        public CrownSolver(IEnumerable<PlayerIdentity> players)
        {
            _players       = new CircularLinkedList<PlayerIdentity>(players);
            _crownedPlayer = _players.First;
        }

        public PlayerIdentity Crowned => _crownedPlayer.Value;

        public PlayerIdentity GiveCrownToNextPlayer()
        {
            _crownedPlayer = _crownedPlayer.Next;
            return _crownedPlayer.Value;
        }

        public Option<PlayerIdentity> GetNextPlayer(PlayerIdentity player)
        {
            return _players.Find(player).Match(current =>
                                               {
                                                   var nextPlayer = current.Next;
                                                   return nextPlayer.Value == _crownedPlayer.Value
                                                       ? OptionExtensions.None<PlayerIdentity>()
                                                       : OptionExtensions.Some(nextPlayer.Value);
                                               },
                                               () => throw new InvalidOperationException(
                                                   $"Player {player} is not in the list."));
        }
    }
}