﻿using System.Collections.Generic;

namespace OriflammeEngine.Hand
{
    /// <summary>
    ///     Represents a solver for hands.
    /// </summary>
    public interface IHandSolver
    {
        /// <summary>
        ///     Get cards from the hand of a player.
        /// </summary>
        /// <param name="player">Player to get the cards from.</param>
        /// <returns>Cards of the player.</returns>
        IEnumerable<CardIdentity> Get(PlayerIdentity player);

        /// <summary>
        ///     Remove a card from the hand of a player.
        /// </summary>
        /// <param name="player">Player to remove the card from.</param>
        /// <param name="cardIdentity">Card to remove.</param>
        void Remove(PlayerIdentity player, CardIdentity cardIdentity);
    }
}