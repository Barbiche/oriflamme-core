﻿using BarbFoundation;

namespace OriflammeEngine.Crown
{
    /// <summary>
    ///     Represents the Crown feature.
    /// </summary>
    public interface ICrownSolver
    {
        /// <summary>
        ///     Returns the crowned player.
        /// </summary>
        PlayerIdentity Crowned { get; }

        /// <summary>
        ///     Give the crown to the next player.
        /// </summary>
        /// <returns>The new crowned player.</returns>
        PlayerIdentity GiveCrownToNextPlayer();

        /// <summary>
        ///     Returns the player which plays after the provided. Returns empty if the provided is the last.
        /// </summary>
        /// <param name="player">Player who played already.</param>
        /// <returns>The next player, empty if there are not players to play anymore.</returns>
        Option<PlayerIdentity> GetNextPlayer(PlayerIdentity player);
    }
}