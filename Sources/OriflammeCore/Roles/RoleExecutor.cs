﻿using System;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Reveal;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public class RoleExecutor : IRoleExecutor
    {
        private readonly IGameStack             _gameStack;
        private readonly IInvestSolver          _investSolver;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly IRevealSolver          _revealSolver;
        private readonly IRoleFactory           _roleFactory;
        private readonly IStateBus              _stateBus;

        public RoleExecutor(IRoleFactory           roleFactory,
                            IPlayerInfluenceSolver playerInfluenceSolver,
                            IStateBus              stateBus,
                            IKillExposer           killBus,
                            IGameStack             gameStack,
                            IInvestSolver          investSolver,
                            IRevealSolver          revealSolver,
                            IPlayerCardsSolver     playerCardsSolver)
        {
            _roleFactory           = roleFactory;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _gameStack             = gameStack;
            _investSolver          = investSolver;
            _revealSolver          = revealSolver;
            _playerCardsSolver     = playerCardsSolver;

            killBus.KillStream.Subscribe(HandleKillRequest);
        }

        public void Execute(CardIdentity card)
        {
            var role = _roleFactory.Create(card);

            // Reveal card
            _revealSolver.Flip(card);
            if (role is IReveal revealRole)
            {
                revealRole.Reveal();
            }
            else
            {
                var influenceGain = _investSolver.Consume(card);
                _playerInfluenceSolver.Give(_playerCardsSolver.Get(card), influenceGain);
                _stateBus.Publish(new PlayerRevealed(_playerCardsSolver.Get(card), influenceGain, card));
            }

            role.Execute();
        }

        private void HandleKillRequest(KillRequest killRequest)
        {
            var killedEntity = killRequest.Killed;
            var role         = _roleFactory.Create(killedEntity);

            if (role is IKill onKillBehavior)
            {
                onKillBehavior.OnKilled(killRequest.Killer);
            }
            else
            {
                _investSolver.Consume(killedEntity);
                _gameStack.RemoveCard(killRequest.Killed);
            }
        }
    }
}