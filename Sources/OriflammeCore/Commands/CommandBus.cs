﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace OriflammeCore.Commands
{
    public sealed class CommandBus : ICommandBus
    {
        private readonly ISubject<ICommand> _commandSubject = new Subject<ICommand>();

        public void Publish(ICommand command)
        {
            _commandSubject.OnNext(command);
        }

        public IObservable<ICommand> CommandStream => _commandSubject.AsObservable();
    }
}