﻿using System.Collections.Generic;

namespace OriflammeEngine.PlayerInfluence
{
    public interface IPlayerInfluenceSolverFactory
    {
        IPlayerInfluenceSolver Create(IEnumerable<PlayerIdentity> players);
    }
}