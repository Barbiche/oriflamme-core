﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EnsureThat;
using OriflammeCore.Commands;
using OriflammeEngine;
using OriflammeEngine.Crown;
using OriflammeEngine.Hand;
using OriflammeEngine.Stack;

namespace OriflammeCore.Game
{
    public class CrownPhase : IGamePhase
    {
        private readonly ICommandBus  _commandBus;
        private readonly ICrownSolver _crownSolver;

        private readonly ISubject<bool> _endOfPhase = new Subject<bool>();
        private readonly IGameStack     _gameStack;
        private readonly IHandSolver    _handSolver;

        public CrownPhase(ICrownSolver crownSolver,
                          IHandSolver  handSolver,
                          IGameStack   gameStack,
                          ICommandBus  commandBus)
        {
            _crownSolver = crownSolver;
            _handSolver  = handSolver;
            _gameStack   = gameStack;
            _commandBus  = commandBus;

            AskPlayerToChooseACard(_crownSolver.Crowned);
        }

        public IObservable<bool> EndOfPhaseStream => _endOfPhase.AsObservable();

        private void AskPlayerToChooseACard(PlayerIdentity player)
        {
            _commandBus.Publish(new ChooseCardFromHand(player,
                                                       _handSolver.Get(player),
                                                       card =>
                                                       {
                                                           _commandBus.Publish(
                                                               new StackPositionForNewCardCommand(
                                                                   player,
                                                                   value => PlayCard(player, card, value)));
                                                       }));
        }

        private void PlayCard(PlayerIdentity player, CardIdentity chosenCard, bool value)
        {
            Ensure.That(chosenCard, nameof(chosenCard)).IsNotNull();

            _handSolver.Remove(player, chosenCard);

            if (value)
            {
                _gameStack.AddCardAtStart(chosenCard);
            }
            else
            {
                _gameStack.AddCardAtEnd(chosenCard);
            }

            _crownSolver.GetNextPlayer(player).Match(AskPlayerToChooseACard, () =>
            {
                _crownSolver.GiveCrownToNextPlayer();
                _endOfPhase.OnCompleted();
            });
        }
    }
}