﻿namespace OriflammeEngine.Reveal
{
    public enum VisibilityState
    {
        Revealed,
        Hidden
    }
}