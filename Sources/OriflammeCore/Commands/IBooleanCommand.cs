﻿namespace OriflammeCore.Commands
{
    /// <summary>
    ///     Represents a request from the core of a boolean value.
    /// </summary>
    public interface IBooleanCommand : ICommand<bool> { }
}