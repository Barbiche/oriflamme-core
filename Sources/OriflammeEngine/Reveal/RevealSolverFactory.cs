﻿using System.Collections.Generic;

namespace OriflammeEngine.Reveal
{
    public class RevealSolverFactory : IRevealSolverFactory
    {
        public IRevealSolver Create(IEnumerable<CardIdentity> cards)
        {
            return new RevealSolver(cards);
        }
    }
}