﻿using System;
using Equ;

namespace OriflammeEngine
{
    /// <summary>
    ///     Represents the identity of a card.
    /// </summary>
    public sealed class CardIdentity : MemberwiseEquatable<CardIdentity>
    {
        private readonly Guid _guid = Guid.NewGuid();

        public static implicit operator Guid(CardIdentity id)
        {
            return id._guid;
        }
    }
}