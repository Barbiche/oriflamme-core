﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace OriflammeCore.States
{
    public sealed class StateBus : IStateBus
    {
        private readonly ISubject<IState> _stateSubject = new Subject<IState>();

        public IObservable<IState> StateStream => _stateSubject.AsObservable();

        public void Publish(IState state)
        {
            _stateSubject.OnNext(state);
        }
    }
}