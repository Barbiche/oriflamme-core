using OriflammeEngine;
using OriflammeEngine.PlayerInfluence;
using Xunit;

namespace OriflammeEngine_Tests.Score
{
    public class ScoreSolverTests
    {
        [Fact]
        public void Get_FromPlayerInInitialPlayers_Returns1()
        {
            var player = new PlayerIdentity();
            var sut    = new PlayerInfluenceSolver(new[] { player });

            Assert.Equal(1, sut.Get(player));
        }
    }
}