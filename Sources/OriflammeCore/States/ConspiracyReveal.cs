﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class ConspiracyReveal : IState
    {
        public ConspiracyReveal(PlayerIdentity player, int influenceGain, CardIdentity revealedConspiracyCard)
        {
            Player                 = player;
            InfluenceGain          = influenceGain;
            RevealedConspiracyCard = revealedConspiracyCard;
        }

        public PlayerIdentity Player                 { get; }
        public int            InfluenceGain          { get; }
        public CardIdentity   RevealedConspiracyCard { get; }
    }
}