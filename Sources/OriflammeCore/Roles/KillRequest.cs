﻿using OriflammeEngine;

namespace OriflammeCore.Roles
{
    public sealed class KillRequest
    {
        public KillRequest(CardIdentity killer, CardIdentity killed)
        {
            Killer = killer;
            Killed = killed;
        }

        public CardIdentity Killer { get; }
        public CardIdentity Killed { get; }
    }
}