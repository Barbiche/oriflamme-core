﻿using OriflammeEngine.PlayerCards;
using OriflammeEngine.Reveal;

namespace OriflammeEngine.Stack
{
    public interface IGameStackFactory
    {
        IGameStack Create(IRevealSolver revealSolver, IPlayerCardsSolver playerCardsSolver);
    }
}