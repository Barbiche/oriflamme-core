﻿using System;
using BarbFoundation;

namespace OriflammeEngine.Stack
{
    public class GameStackPointer : IGameStackPointer
    {
        private readonly IGameStack           _gameStack;
        private          Option<CardIdentity> _currentIdentity = OptionExtensions.None<CardIdentity>();

        public GameStackPointer(IGameStack gameStack, ICardRemovedExposer cardRemovedExposer)
        {
            _gameStack = gameStack;

            cardRemovedExposer.RemovedCardUpdates.Subscribe(HandlePointerOnRemove);
        }

        public Option<CardIdentity> PointedCard
        {
            get
            {
                _currentIdentity.WhenSome(currId =>
                {
                    _gameStack.InternalStack.Get(currId)
                        .Proj(() => throw new InvalidOperationException(
                                  "Id is not in stack anymore."));
                });
                return _currentIdentity;
            }
        }

        public Option<CardIdentity> MoveForward()
        {
            _currentIdentity.Match(currId =>
            {
                _gameStack.InternalStack.Get(currId).Match(node =>
                {
                    // id is present, we take the next one.
                    var next = node.Next;
                    if (next == null)
                    {
                        // We reached end of the stack.
                        _currentIdentity = OptionExtensions.None<CardIdentity>();
                    }
                    else
                    {
                        // Next exists, we move to it.
                        _currentIdentity = OptionExtensions.Some(next.Value);
                    }
                }, () => throw new InvalidOperationException($"{currId} not in stack anymore."));
            }, () =>
            {
                // Card is not yet defined, we take the first card of the stack.
                _currentIdentity = _gameStack.GetFirst();
            });
            return _currentIdentity;
        }

        public Option<CardIdentity> MoveBackward()
        {
            _currentIdentity.Match(currId =>
            {
                _gameStack.InternalStack.Get(currId).Match(node =>
                {
                    // id is present, we take the previous one.
                    var previous = node.Previous;
                    if (previous == null)
                    {
                        // We reached beginning of the stack.
                        _currentIdentity = OptionExtensions.None<CardIdentity>();
                    }
                    else
                    {
                        // Previous exists, we move to it.
                        _currentIdentity = OptionExtensions.Some(previous.Value);
                    }
                }, () => throw new InvalidOperationException($"{currId} not in stack anymore."));
            }, () =>
            {
                // Card is not yet defined, we take the last card of the stack.
                _currentIdentity = _gameStack.GetLast();
            });
            return _currentIdentity;
        }

        private void HandlePointerOnRemove(CardIdentity removed)
        {
            _currentIdentity.WhenSome(currId =>
            {
                if (removed == currId)
                {
                    // The pointed card was removed. We move backward.
                    MoveBackward();
                }
            });
        }
    }
}