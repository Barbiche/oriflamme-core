﻿using BarbFoundation;
using OriflammeCore.Commands;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public sealed class RoyalDecree : IRole
    {
        private readonly ICommandBus        _commandBus;
        private readonly IGameStack         _gameStack;
        private readonly IPlayerCardsSolver _playerCardsSolver;
        private readonly CardIdentity       _roleCard;
        private readonly IStateBus          _stateBus;

        public RoyalDecree(CardIdentity       roleCard,
                           IGameStack         gameStack,
                           ICommandBus        commandBus,
                           IStateBus          stateBus,
                           IPlayerCardsSolver playerCardsSolver)
        {
            _roleCard          = roleCard;
            _gameStack         = gameStack;
            _commandBus        = commandBus;
            _stateBus          = stateBus;
            _playerCardsSolver = playerCardsSolver;
        }

        public void Execute()
        {
            // Request the card to move
            _commandBus.Publish(new RoyalDecreeCardCommand(_playerCardsSolver.Get(_roleCard), _roleCard,
                                                           _gameStack.GetPlayerCardsExcepted(
                                                               _playerCardsSolver.Get(_roleCard), _roleCard),
                                                           RequestNewPosition));
        }

        private void RequestNewPosition(CardIdentity cardToMove)
        {
            // Request the new position for the card
            _commandBus.Publish(new RoyalDecreePositionCommand(_playerCardsSolver.Get(_roleCard),
                                                               _roleCard,
                                                               _gameStack.GetCardsExcepted(cardToMove),
                                                               pivot => MoveCard(cardToMove, pivot)));
        }

        private void MoveCard(CardIdentity card, Option<CardIdentity> pivotCard)
        {
            pivotCard.Match(pivot => { _gameStack.MoveAfter(card, pivot); }, () => { _gameStack.MoveAtStart(card); });

            _gameStack.RemoveCard(_roleCard);

            _stateBus.Publish(new RoyalDecreeState(_playerCardsSolver.Get(_roleCard), card));
        }
    }
}