﻿using System;

namespace OriflammeCore.States
{
    public interface IStateExposer
    {
        /// <summary>
        ///     Observable stream of state.
        /// </summary>
        IObservable<IState> StateStream { get; }
    }
}