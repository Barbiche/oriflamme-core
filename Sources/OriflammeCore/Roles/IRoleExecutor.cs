﻿using OriflammeEngine;

namespace OriflammeCore.Roles
{
    public interface IRoleExecutor
    {
        void Execute(CardIdentity card);
    }
}