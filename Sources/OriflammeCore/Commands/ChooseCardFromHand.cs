﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     A request to a player to choose a card from his hand.
    /// </summary>
    public class ChooseCardFromHand : ICardBetweenManyCommand
    {
        private readonly Action<CardIdentity> _callback;

        public ChooseCardFromHand(PlayerIdentity       waitedInputPlayer, IEnumerable<CardIdentity> availableCards,
                                  Action<CardIdentity> callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public ImmutableArray<CardIdentity> AvailableCards { get; }

        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(CardIdentity result)
        {
            _callback.Invoke(result);
        }
    }
}