﻿using System;

namespace OriflammeCore.Roles
{
    public interface IKillExposer
    {
        IObservable<KillRequest> KillStream { get; }
    }
}