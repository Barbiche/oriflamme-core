﻿namespace OriflammeCore.States
{
    /// <summary>
    ///     Represents an event bus for <see cref="IState" />
    /// </summary>
    public interface IStateBus : IStateExposer
    {
        /// <summary>
        ///     Publish a <see cref="IState" /> in the bus.
        /// </summary>
        /// <param name="state">state to publish.</param>
        void Publish(IState state);
    }
}