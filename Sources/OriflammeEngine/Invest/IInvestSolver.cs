﻿namespace OriflammeEngine.Invest
{
    /// <summary>
    ///     Represents a solver for investments.
    /// </summary>
    public interface IInvestSolver
    {
        /// <summary>
        ///     Invest on a card.
        /// </summary>
        /// <param name="card">Card to invest on.</param>
        /// <param name="influence">Nb of influences to invest.</param>
        void Invest(CardIdentity card, int influence);

        /// <summary>
        ///     Remove all invested influences on the card.
        /// </summary>
        /// <param name="card">Card to consume.</param>
        /// <returns>The influences that have been invested.</returns>
        int Consume(CardIdentity card);
    }
}