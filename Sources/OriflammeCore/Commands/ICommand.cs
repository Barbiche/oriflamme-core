﻿using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     Represent a request from the core of a user choice.
    /// </summary>
    public interface ICommand<in T> : ICommand
    {
        /// <summary>
        ///     The player which is requested to input.
        /// </summary>
        PlayerIdentity WaitedInputPlayer { get; }

        /// <summary>
        ///     Sends the result of the command.
        /// </summary>
        /// <param name="result">Result of the command.</param>
        void SendResult(T result);
    }

    /// <summary>
    ///     Marker for commands.
    /// </summary>
    public interface ICommand { }
}