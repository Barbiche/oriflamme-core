﻿using System.Collections.Generic;

namespace OriflammeEngine.Role
{
    public interface IRoleSolverFactory
    {
        IRoleSolver Create(IEnumerable<RoleCard> cards);
    }

    public record RoleCard(CardIdentity CardIdentity, Role Role);
}