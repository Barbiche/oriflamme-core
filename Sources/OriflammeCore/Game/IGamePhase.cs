﻿using System;

namespace OriflammeCore.Game
{
    public interface IGamePhase
    {
        IObservable<bool> EndOfPhaseStream { get; }
    }
}