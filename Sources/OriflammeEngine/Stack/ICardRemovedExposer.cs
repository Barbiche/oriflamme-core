﻿using System;

namespace OriflammeEngine.Stack
{
    public interface ICardRemovedExposer
    {
        IObservable<CardIdentity> RemovedCardUpdates { get; }
    }
}