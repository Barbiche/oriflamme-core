﻿using System.Collections.Generic;
using System.Linq;
using BarbFoundation;
using OriflammeCore.Commands;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public class Spy : IRole
    {
        private readonly ICommandBus            _commandBus;
        private readonly IGameStack             _gameStack;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IStateBus              _stateBus;

        public Spy(CardIdentity           roleCard,
                   IPlayerInfluenceSolver playerInfluenceSolver,
                   IGameStack             gameStack,
                   ICommandBus            commandBus,
                   IStateBus              stateBus,
                   IPlayerCardsSolver     playerCardsSolver)
        {
            _roleCard              = roleCard;
            _playerInfluenceSolver = playerInfluenceSolver;
            _gameStack             = gameStack;
            _commandBus            = commandBus;
            _stateBus              = stateBus;
            _playerCardsSolver     = playerCardsSolver;
        }

        public void Execute()
        {
            var stealablePlayers = new HashSet<PlayerIdentity>();
            _gameStack.GetNext(_roleCard).WhenSome(nextCard =>
            {
                if (_playerCardsSolver.Get(nextCard)                             != _playerCardsSolver.Get(_roleCard) &&
                    _playerInfluenceSolver.Get(_playerCardsSolver.Get(nextCard)) > 0)
                {
                    stealablePlayers.Add(_playerCardsSolver.Get(nextCard));
                }
            });
            _gameStack.GetPrevious(_roleCard).WhenSome(previousCard =>
            {
                if (_playerCardsSolver.Get(previousCard) != _playerCardsSolver.Get(_roleCard) &&
                    _playerInfluenceSolver.Get(_playerCardsSolver.Get(previousCard)) > 0)
                {
                    stealablePlayers.Add(_playerCardsSolver.Get(previousCard));
                }
            });

            if (stealablePlayers.Any())
            {
                _commandBus.Publish(new SpyCommand(_playerCardsSolver.Get(_roleCard), _roleCard, stealablePlayers,
                                                   StealFrom));
            }
            else
            {
                _stateBus.Publish(new SpyState(_roleCard, OptionExtensions.None<PlayerIdentity>()));
            }
        }

        private void StealFrom(PlayerIdentity player)
        {
            var changes = new[]
            {
                new InfluenceChange(_playerCardsSolver.Get(_roleCard), 1),
                new InfluenceChange(player, -1)
            };

            _playerInfluenceSolver.ApplyChanges(changes);
            _stateBus.Publish(new SpyState(_roleCard, OptionExtensions.Some(player)));
        }
    }
}