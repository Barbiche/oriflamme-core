﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class PlayerInvested : IState
    {
        public PlayerInvested(PlayerIdentity player, int newValue)
        {
            Player   = player;
            NewValue = newValue;
        }

        public PlayerIdentity Player   { get; }
        public int            NewValue { get; }
    }
}