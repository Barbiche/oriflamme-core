﻿using System;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     A request to a player to choose a stack position for a new card.
    /// </summary>
    public class StackPositionForNewCardCommand : IBooleanCommand
    {
        private readonly Action<bool> _callback;

        public StackPositionForNewCardCommand(PlayerIdentity waitedInputPlayer, Action<bool> callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
        }

        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(bool result)
        {
            _callback.Invoke(result);
        }
    }
}