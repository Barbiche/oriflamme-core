﻿using OriflammeEngine;
using OriflammeEngine.Role;

namespace OriflammeCore.Roles
{
    /// <summary>
    ///     Represents a factory for <see cref="IRole" />.
    /// </summary>
    public interface IRoleFactory
    {
        /// <summary>
        ///     Creates the appropriated <see cref="IRole" />
        /// </summary>
        /// <param name="card">Card having the role.</param>
        /// <returns></returns>
        IRole Create(CardIdentity card);

        /// <summary>
        ///     Creates from a specific role and a card to handle it.
        /// </summary>
        /// <param name="role"></param>
        /// <param name="card"></param>
        /// <returns></returns>
        IRole Create(Role role, CardIdentity card);
    }
}