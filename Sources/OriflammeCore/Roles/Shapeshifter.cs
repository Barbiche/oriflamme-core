﻿using System.Collections.Generic;
using OriflammeCore.Commands;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Reveal;
using OriflammeEngine.Role;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public sealed class Shapeshifter : IRole
    {
        private readonly ICommandBus            _commandBus;
        private readonly IGameStack             _gameStack;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IRevealSolver          _revealSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IRoleFactory           _roleFactory;
        private readonly IRoleSolver            _roleSolver;
        private readonly IStateBus              _stateBus;
        private          IPlayerInfluenceSolver _playerInfluenceSolver;

        public Shapeshifter(CardIdentity           roleCard,
                            IPlayerInfluenceSolver playerInfluenceSolver,
                            IStateBus              stateBus,
                            ICommandBus            commandBus,
                            IGameStack             gameStack,
                            IRoleFactory           roleFactory,
                            IRevealSolver          revealSolver,
                            IPlayerCardsSolver     playerCardsSolver,
                            IRoleSolver            roleSolver)
        {
            _roleCard              = roleCard;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _commandBus            = commandBus;
            _gameStack             = gameStack;
            _roleFactory           = roleFactory;
            _revealSolver          = revealSolver;
            _playerCardsSolver     = playerCardsSolver;
            _roleSolver            = roleSolver;
        }

        public void Execute()
        {
            var availableCards = new HashSet<CardIdentity>();
            _gameStack.GetNext(_roleCard).WhenSome(nextCard =>
            {
                if (_revealSolver.Get(nextCard) is VisibilityState.Revealed)
                {
                    availableCards.Add(nextCard);
                }
            });
            _gameStack.GetPrevious(_roleCard).WhenSome(previousCard =>
            {
                if (_revealSolver.Get(previousCard) is VisibilityState.Revealed)
                {
                    availableCards.Add(previousCard);
                }
            });

            _commandBus.Publish(new ShapeshifterCommand(_playerCardsSolver.Get(_roleCard), _roleCard, availableCards,
                                                        Shapeshift));
        }

        private void Shapeshift(CardIdentity targetCard)
        {
            _stateBus.Publish(new ShapeshifterState(_roleCard, targetCard));

            var targetRole = _roleFactory.Create(_roleSolver.Get(targetCard), _roleCard);
            targetRole.Execute();
        }
    }
}