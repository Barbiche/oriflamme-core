﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class AmbushReveal : IState
    {
        public AmbushReveal(PlayerIdentity player, int influenceGain, CardIdentity revealedAmbush)
        {
            Player         = player;
            InfluenceGain  = influenceGain;
            RevealedAmbush = revealedAmbush;
        }

        public PlayerIdentity Player         { get; }
        public int            InfluenceGain  { get; }
        public CardIdentity   RevealedAmbush { get; }
    }
}