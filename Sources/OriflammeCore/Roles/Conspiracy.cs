﻿using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public sealed class Conspiracy : IRole, IReveal
    {
        private readonly IGameStack             _gameStack;
        private readonly IInvestSolver          _investSolver;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IStateBus              _stateBus;

        public Conspiracy(CardIdentity           roleCard,
                          IGameStack             gameStack,
                          IPlayerInfluenceSolver playerInfluenceSolver,
                          IStateBus              stateBus,
                          IInvestSolver          investSolver,
                          IPlayerCardsSolver     playerCardsSolver)
        {
            _roleCard              = roleCard;
            _gameStack             = gameStack;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _investSolver          = investSolver;
            _playerCardsSolver     = playerCardsSolver;
        }

        public void Reveal()
        {
            var influenceGain = _investSolver.Consume(_roleCard) * 2;
            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), influenceGain);
            _stateBus.Publish(new ConspiracyReveal(_playerCardsSolver.Get(_roleCard), influenceGain, _roleCard));
        }

        public void Execute()
        {
            _gameStack.RemoveCard(_roleCard);
        }
    }
}