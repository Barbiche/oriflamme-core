﻿using System.Collections.Generic;
using EnsureThat;

namespace OriflammeEngine.PlayerInfluence
{
    internal sealed class PlayerInfluenceSolver : IPlayerInfluenceSolver
    {
        private readonly IDictionary<PlayerIdentity, int> _playerScores = new Dictionary<PlayerIdentity, int>();

        public PlayerInfluenceSolver(IEnumerable<PlayerIdentity> players)
        {
            foreach (var player in players)
            {
                _playerScores.Add(player, 1);
            }
        }

        /// <inheritdoc />
        public void ApplyChanges(IEnumerable<InfluenceChange> influenceChanges)
        {
            foreach (var change in influenceChanges)
            {
                ApplyChanges(change);
            }
        }

        /// <inheritdoc />
        public void ApplyChanges(InfluenceChange influenceChange)
        {
            var (player, change) = influenceChange;
            Ensure.That(_playerScores.ContainsKey(player)).IsTrue();
            _playerScores[player] += change;
        }

        /// <inheritdoc />
        public int Get(PlayerIdentity player)
        {
            Ensure.That(_playerScores.ContainsKey(player)).IsTrue();

            return _playerScores[player];
        }

        /// <inheritdoc />
        public int Give(PlayerIdentity player, int influenceGain)
        {
            Ensure.That(_playerScores.ContainsKey(player)).IsTrue();

            _playerScores[player] += influenceGain;

            return _playerScores[player];
        }
    }
}