﻿using System.Collections.Generic;

namespace OriflammeEngine.Invest
{
    public interface IInvestSolverFactory
    {
        IInvestSolver Create(IEnumerable<CardIdentity> cards);
    }
}