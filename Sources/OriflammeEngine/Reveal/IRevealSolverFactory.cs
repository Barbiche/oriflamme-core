﻿using System.Collections.Generic;

namespace OriflammeEngine.Reveal
{
    public interface IRevealSolverFactory
    {
        IRevealSolver Create(IEnumerable<CardIdentity> cards);
    }
}