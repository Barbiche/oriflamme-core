﻿namespace OriflammeCore.Roles
{
    public interface IRole
    {
        void Execute();
    }
}