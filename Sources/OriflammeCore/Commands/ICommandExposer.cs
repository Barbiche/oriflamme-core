﻿using System;

namespace OriflammeCore.Commands
{
    public interface ICommandExposer
    {
        IObservable<ICommand> CommandStream { get; }
    }
}