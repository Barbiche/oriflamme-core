﻿using System.Collections.Generic;
using System.Linq;
using BarbFoundation;
using OriflammeCore.Commands;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public sealed class Soldier : IRole
    {
        private readonly ICommandBus            _commandBus;
        private readonly IGameStack             _gameStack;
        private readonly IKillBus               _killBus;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IStateBus              _stateBus;

        public Soldier(CardIdentity           roleCard,
                       IGameStack             gameStack,
                       IPlayerInfluenceSolver playerInfluenceSolver,
                       IStateBus              stateBus,
                       ICommandBus            commandBus,
                       IKillBus               killBus,
                       IPlayerCardsSolver     playerCardsSolver)
        {
            _roleCard              = roleCard;
            _gameStack             = gameStack;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _commandBus            = commandBus;
            _killBus               = killBus;
            _playerCardsSolver     = playerCardsSolver;
        }

        public void Execute()
        {
            var killableCards = new HashSet<CardIdentity>();
            _gameStack.GetNext(_roleCard).WhenSome(nextCard => { killableCards.Add(nextCard); });
            _gameStack.GetPrevious(_roleCard).WhenSome(previousCard => { killableCards.Add(previousCard); });

            if (killableCards.Any())
            {
                _commandBus.Publish(new SoldierCommand(_playerCardsSolver.Get(_roleCard), _roleCard, killableCards,
                                                       Kill));
            }
            else
            {
                _stateBus.Publish(new SoldierState(_roleCard, OptionExtensions.None<CardIdentity>()));
            }
        }

        private void Kill(CardIdentity cardToKill)
        {
            _stateBus.Publish(new SoldierState(_roleCard, OptionExtensions.Some(cardToKill)));

            _killBus.RequestKill(_roleCard, cardToKill);

            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), 1);
        }
    }
}