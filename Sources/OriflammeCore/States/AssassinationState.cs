﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class AssassinationState : IState
    {
        public AssassinationState(CardIdentity assassinationCard, CardIdentity killedCard)
        {
            AssassinationCard = assassinationCard;
            KilledCard        = killedCard;
        }

        public CardIdentity AssassinationCard { get; }
        public CardIdentity KilledCard        { get; }
    }
}