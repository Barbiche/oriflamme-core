﻿using BarbFoundation;
using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class SpyState : IState
    {
        public SpyState(CardIdentity spyCard, Option<PlayerIdentity> stealthPlayer)
        {
            SpyCard       = spyCard;
            StealthPlayer = stealthPlayer;
        }

        public CardIdentity           SpyCard       { get; }
        public Option<PlayerIdentity> StealthPlayer { get; }
    }
}