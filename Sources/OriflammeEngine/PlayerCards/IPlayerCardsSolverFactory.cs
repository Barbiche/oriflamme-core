﻿using System.Collections.Generic;

namespace OriflammeEngine.PlayerCards
{
    public interface IPlayerCardsSolverFactory
    {
        IPlayerCardsSolver Create(IEnumerable<PlayerCard> playerCards);
    }

    public record PlayerCard(CardIdentity CardIdentity, PlayerIdentity PlayerIdentity);
}