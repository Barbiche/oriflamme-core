﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using BarbFoundation;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public class RoyalDecreePositionCommand : IOptionalCardBetweenManyCommand
    {
        private readonly Action<Option<CardIdentity>> _callback;

        public RoyalDecreePositionCommand(PlayerIdentity               waitedInputPlayer, CardIdentity royalDecreeCard,
                                          IEnumerable<CardIdentity>    availableCards,
                                          Action<Option<CardIdentity>> callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            RoyalDecreeCard   = royalDecreeCard;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public CardIdentity RoyalDecreeCard { get; }

        public ImmutableArray<CardIdentity> AvailableCards    { get; }
        public PlayerIdentity               WaitedInputPlayer { get; }

        public void SendResult(Option<CardIdentity> result)
        {
            _callback(result);
        }
    }
}