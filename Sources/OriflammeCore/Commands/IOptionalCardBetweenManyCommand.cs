﻿using System.Collections.Immutable;
using BarbFoundation;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     Represents a request from the core of a card between a set of cards.
    /// </summary>
    public interface IOptionalCardBetweenManyCommand : ICommand<Option<CardIdentity>>
    {
        /// <summary>
        ///     Available cards to choose from.
        /// </summary>
        ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}