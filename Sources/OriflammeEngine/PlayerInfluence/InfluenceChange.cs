﻿using Equ;

namespace OriflammeEngine.PlayerInfluence
{
    /// <summary>
    ///     Represents a change of influence for a player.
    /// </summary>
    public sealed class InfluenceChange : MemberwiseEquatable<InfluenceChange>
    {
        /// <summary>
        ///     Constructor with a player and the change value.
        /// </summary>
        /// <param name="player">Player concerned by the change.</param>
        /// <param name="change">Influence change.</param>
        public InfluenceChange(PlayerIdentity player, int change)
        {
            Player = player;
            Change = change;
        }

        /// <summary>
        ///     Player concerned by the change.
        /// </summary>
        public PlayerIdentity Player { get; }

        /// <summary>
        ///     Change value. Can be positive, negative or zero.
        /// </summary>
        public int Change { get; }

        public void Deconstruct(out PlayerIdentity player, out int change)
        {
            player = Player;
            change = Change;
        }
    }
}