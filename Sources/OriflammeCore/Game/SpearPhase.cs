﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using OriflammeCore.Commands;
using OriflammeCore.Roles;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Reveal;
using OriflammeEngine.Stack;

namespace OriflammeCore.Game
{
    public class SpearPhase : IGamePhase
    {
        private readonly ICommandBus            _commandBus;
        private readonly ISubject<bool>         _endOfPhase = new Subject<bool>();
        private readonly IInvestSolver          _investSolver;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly IRevealSolver          _revealSolver;
        private readonly IRoleExecutor          _roleExecutor;

        private readonly IGameStackPointer _stackPointer;
        private readonly IStateBus         _stateBus;

        public SpearPhase(IPlayerInfluenceSolver playerInfluenceSolver,
                          IRoleExecutor          roleExecutor,
                          ICommandBus            commandBus,
                          IStateBus              stateBus,
                          IGameStackPointer      gameStackPointer,
                          IInvestSolver          investSolver,
                          IRevealSolver          revealSolver,
                          IPlayerCardsSolver     playerCardsSolver)
        {
            _playerInfluenceSolver = playerInfluenceSolver;
            _roleExecutor          = roleExecutor;
            _commandBus            = commandBus;
            _stateBus              = stateBus;

            _stackPointer      = gameStackPointer;
            _investSolver      = investSolver;
            _revealSolver      = revealSolver;
            _playerCardsSolver = playerCardsSolver;

            ProcessStack();
        }

        public IObservable<bool> EndOfPhaseStream => _endOfPhase.AsObservable();

        private void ProcessStack()
        {
            _stackPointer.MoveForward().Match(card =>
            {
                if (_revealSolver.Get(card) is VisibilityState.Revealed)
                {
                    _roleExecutor.Execute(card);
                    ProcessStack();
                }
                else
                {
                    _commandBus.Publish(new ChooseToRevealCard(_playerCardsSolver.Get(card), card, value =>
                    {
                        Reveal(card, value);
                        ProcessStack();
                    }));
                }
            }, () => { _endOfPhase.OnCompleted(); });
        }

        private void Reveal(CardIdentity cardEntity, bool toReveal)
        {
            if (toReveal)
            {
                _roleExecutor.Execute(cardEntity);
            }
            else
            {
                // Invest
                _investSolver.Invest(cardEntity, 1);
                _stateBus.Publish(new PlayerInvested(_playerCardsSolver.Get(cardEntity),
                                                     _playerInfluenceSolver.Get(_playerCardsSolver.Get(cardEntity))));
            }
        }
    }
}