﻿using System.Collections.Generic;

namespace OriflammeEngine.Crown
{
    public interface ICrownSolverFactory
    {
        ICrownSolver Create(IEnumerable<PlayerIdentity> players);
    }
}