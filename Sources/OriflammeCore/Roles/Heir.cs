﻿using System.Linq;
using BarbFoundation;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Role;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public class Heir : IRole
    {
        private readonly IGameStack             _gameStack;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IRoleSolver            _roleSolver;
        private readonly IStateBus              _stateBus;

        public Heir(CardIdentity           roleCard,
                    IPlayerInfluenceSolver playerInfluenceSolver,
                    IGameStack             gameStack,
                    IStateBus              stateBus,
                    IPlayerCardsSolver     playerCardsSolver,
                    IRoleSolver            roleSolver)
        {
            _roleCard              = roleCard;
            _playerInfluenceSolver = playerInfluenceSolver;
            _gameStack             = gameStack;
            _stateBus              = stateBus;
            _playerCardsSolver     = playerCardsSolver;
            _roleSolver            = roleSolver;
        }

        public void Execute()
        {
            if (_gameStack.RevealedCards.Any(card => _roleSolver.Get(card) == _roleSolver.Get(_roleCard) &&
                                                     card                  != _roleCard))
            {
                _stateBus.Publish(new HeirState(OptionExtensions.None<PlayerIdentity>()));
                return;
            }

            _stateBus.Publish(new HeirState(OptionExtensions.Some(_playerCardsSolver.Get(_roleCard))));
            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), 2);
        }
    }
}