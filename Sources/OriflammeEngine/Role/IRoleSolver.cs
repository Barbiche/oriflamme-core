﻿namespace OriflammeEngine.Role
{
    /// <summary>
    ///     Represent a solver for roles in the game.
    /// </summary>
    public interface IRoleSolver
    {
        /// <summary>
        ///     Get the role of a card.
        /// </summary>
        /// <param name="cardIdentity">Card to get the role from.</param>
        /// <returns>Role associated to the card.</returns>
        Role Get(CardIdentity cardIdentity);

        /// <summary>
        ///     Give a role to a card.
        /// </summary>
        /// <param name="cardIdentity">Card to give the role.</param>
        /// <param name="role">Role to give to the card</param>
        void Give(CardIdentity cardIdentity, Role role);
    }
}