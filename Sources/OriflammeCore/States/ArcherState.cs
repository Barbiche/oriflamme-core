﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class ArcherState : IState
    {
        public ArcherState(CardIdentity archerCard, CardIdentity killedCard)
        {
            ArcherCard = archerCard;
            KilledCard = killedCard;
        }

        public CardIdentity ArcherCard { get; }
        public CardIdentity KilledCard { get; }
    }
}