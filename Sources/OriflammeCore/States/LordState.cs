﻿using BarbFoundation;
using OriflammeEngine;

namespace OriflammeCore.States
{
    public class LordState : IState
    {
        public LordState(CardIdentity         lordCard, int influenceGain, Option<CardIdentity> leftCard,
                         Option<CardIdentity> rightCard)
        {
            InfluenceGain = influenceGain;
            LeftCard      = leftCard;
            RightCard     = rightCard;
            LordCard      = lordCard;
        }

        public int                  InfluenceGain { get; }
        public Option<CardIdentity> LeftCard      { get; }
        public Option<CardIdentity> RightCard     { get; }
        public CardIdentity         LordCard      { get; }
    }
}