﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarbFoundation;
using OriflammeCore.Commands;
using OriflammeCore.Roles;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Crown;
using OriflammeEngine.Hand;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Reveal;
using OriflammeEngine.Stack;

namespace OriflammeCore.Game
{
    public class Game : IGame
    {
        private readonly ICommandBus               _commandBus;
        private readonly ICrownSolver              _crownSolver;
        private readonly IGameStackPointerFactory  _gameStackPointerFactory;
        private readonly IInvestSolver             _investSolver;
        private readonly IPlayerCardsSolver        _playerCardsSolver;
        private readonly IPlayerInfluenceSolver    _playerInfluenceSolver;
        private readonly PlayerIdentity[]          _players;
        private readonly IRevealSolver             _revealSolver;
        private readonly IRoleExecutor             _roleExecutor;
        private readonly IStateBus                 _stateBus;
        private          Option<IGameStackPointer> _gameStackPointer = OptionExtensions.None<IGameStackPointer>();


        private bool _isLastRound;

        public Game(IEnumerable<PlayerIdentity> players,
                    IGameStack                  gameStack,
                    IStateBus                   stateBus,
                    ICommandBus                 commandBus,
                    IPlayerInfluenceSolver      playerInfluenceSolver,
                    IHandSolver                 handSolver,
                    IRoleExecutor               roleExecutor,
                    ICrownSolver                crownSolver,
                    IGameStackPointerFactory    gameStackPointerFactory,
                    IInvestSolver               investSolver,
                    IRevealSolver               revealSolver,
                    IPlayerCardsSolver          playerCardsSolver)
        {
            _stateBus                = stateBus;
            _commandBus              = commandBus;
            _players                 = players.ToArray();
            HandSolver               = handSolver;
            _playerInfluenceSolver   = playerInfluenceSolver;
            _roleExecutor            = roleExecutor;
            _crownSolver             = crownSolver;
            _gameStackPointerFactory = gameStackPointerFactory;
            _investSolver            = investSolver;
            _revealSolver            = revealSolver;
            _playerCardsSolver       = playerCardsSolver;

            GameStack = gameStack;
        }

        public IEnumerable<PlayerIdentity> Players => _players;

        public IGameStack           GameStack  { get; }
        public IHandSolver          HandSolver { get; }
        public Option<CardIdentity> Current    => _gameStackPointer.Bind(pointer => pointer.PointedCard);

        public void StartGame()
        {
            CreateCrownPhase();
        }

        public IObservable<IState> StateStream => _stateBus.StateStream;

        public IObservable<ICommand> CommandStream => _commandBus.CommandStream;

        private void CreateCrownPhase()
        {
            DetermineIfLastRound();

            _stateBus.Publish(new SwitchingToCrownPhase());

            var crownPhase = new CrownPhase(_crownSolver, HandSolver, GameStack, _commandBus);

            crownPhase.EndOfPhaseStream.Subscribe(b => { }, CreateSpearPhase);
        }

        private void DetermineIfLastRound()
        {
            if (HandSolver.Get(_players.First()).Count() <= 2)
            {
                _isLastRound = true;
                _stateBus.Publish(new LastRound());
            }
        }

        private void CreateSpearPhase()
        {
            _stateBus.Publish(new SwitchingToSpearPhase());

            var pointer = _gameStackPointerFactory.Create();
            _gameStackPointer = OptionExtensions.Some(pointer);
            var spearPhase = new SpearPhase(_playerInfluenceSolver,
                                            _roleExecutor,
                                            _commandBus,
                                            _stateBus,
                                            pointer,
                                            _investSolver,
                                            _revealSolver,
                                            _playerCardsSolver);

            spearPhase.EndOfPhaseStream.Subscribe(b => { }, () =>
            {
                if (_isLastRound)
                {
                    EndGame();
                }
                else
                {
                    CreateCrownPhase();
                }
            });
        }

        private void EndGame()
        {
            _stateBus.Publish(new EndGame(Players.Select(player => (player, _playerInfluenceSolver.Get(player)))));
        }
    }
}