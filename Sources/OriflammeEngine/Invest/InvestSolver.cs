﻿using System.Collections.Generic;
using EnsureThat;

namespace OriflammeEngine.Invest
{
    internal sealed class InvestSolver : IInvestSolver
    {
        private readonly IDictionary<CardIdentity, int> _investments = new Dictionary<CardIdentity, int>();

        public InvestSolver(IEnumerable<CardIdentity> cards)
        {
            foreach (var card in cards)
            {
                _investments.Add(card, 0);
            }
        }

        public void Invest(CardIdentity card, int influence)
        {
            Ensure.That(_investments.ContainsKey(card)).IsTrue();
            _investments[card] += influence;
        }

        public int Consume(CardIdentity card)
        {
            Ensure.That(_investments.ContainsKey(card)).IsTrue();
            var count = _investments[card];
            _investments[card] = 0;
            return count;
        }
    }
}