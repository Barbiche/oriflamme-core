﻿using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public sealed class Ambush : IRole, IReveal, IKill
    {
        private readonly IGameStack             _gameStack;
        private readonly IInvestSolver          _investSolver;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IStateBus              _stateBus;

        public Ambush(CardIdentity           roleCard,
                      IGameStack             gameStack,
                      IPlayerInfluenceSolver playerInfluenceSolver,
                      IStateBus              stateBus,
                      IInvestSolver          investSolver,
                      IPlayerCardsSolver     playerCardsSolver)
        {
            _roleCard              = roleCard;
            _gameStack             = gameStack;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _investSolver          = investSolver;
            _playerCardsSolver     = playerCardsSolver;
        }

        public bool OnKilled(CardIdentity killerCard)
        {
            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), 4);

            // RequestKill the killer card
            _gameStack.RemoveCard(killerCard);
            _investSolver.Consume(killerCard);

            _gameStack.RemoveCard(_roleCard);

            _stateBus.Publish(new AmbushKilled
            {
                AmbushCard   = _roleCard,
                AmbushPlayer = _playerCardsSolver.Get(_roleCard),
                KillerCard   = killerCard
            });

            return true;
        }

        public void Reveal()
        {
            _investSolver.Consume(_roleCard);
            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), 1);
            _stateBus.Publish(new AmbushReveal(_playerCardsSolver.Get(_roleCard), 1, _roleCard));
        }

        public void Execute()
        {
            _gameStack.RemoveCard(_roleCard);
        }
    }
}