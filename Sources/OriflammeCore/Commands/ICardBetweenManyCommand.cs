﻿using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     Represents a request from the core of a card between a set of cards.
    /// </summary>
    public interface ICardBetweenManyCommand : ICommand<CardIdentity>
    {
        /// <summary>
        ///     Available cards to choose from.
        /// </summary>
        ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}