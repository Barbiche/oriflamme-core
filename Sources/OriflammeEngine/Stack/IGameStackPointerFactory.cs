﻿namespace OriflammeEngine.Stack
{
    public interface IGameStackPointerFactory
    {
        IGameStackPointer Create();
    }
}