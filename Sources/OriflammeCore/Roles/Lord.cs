﻿using BarbFoundation;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public class Lord : IRole
    {
        private readonly IGameStack             _gameStack;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IStateBus              _stateBus;

        public Lord(CardIdentity           roleCard,
                    IPlayerInfluenceSolver playerInfluenceSolver,
                    IStateBus              stateBus,
                    IGameStack             gameStack,
                    IPlayerCardsSolver     playerCardsSolver)
        {
            _roleCard              = roleCard;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _gameStack             = gameStack;
            _playerCardsSolver     = playerCardsSolver;
        }

        public void Execute()
        {
            var influenceGain          = 1;
            var nextCardSamePlayer     = OptionExtensions.None<CardIdentity>();
            var previousCardSamePlayer = OptionExtensions.None<CardIdentity>();
            _gameStack.GetNext(_roleCard).WhenSome(nextCard =>
            {
                if (_playerCardsSolver.Get(nextCard) == _playerCardsSolver.Get(_roleCard))
                {
                    nextCardSamePlayer = OptionExtensions.Some(nextCard);
                    influenceGain++;
                }
            });
            _gameStack.GetPrevious(_roleCard).WhenSome(previousCard =>
            {
                if (_playerCardsSolver.Get(previousCard) == _playerCardsSolver.Get(_roleCard))
                {
                    previousCardSamePlayer = OptionExtensions.Some(previousCard);
                    influenceGain++;
                }
            });

            _stateBus.Publish(new LordState(_roleCard, influenceGain,
                                            previousCardSamePlayer,
                                            nextCardSamePlayer));
            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), influenceGain);
        }
    }
}