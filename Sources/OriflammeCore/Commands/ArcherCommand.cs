﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public sealed class ArcherCommand : ICardBetweenManyCommand
    {
        private readonly Action<CardIdentity> _callback;

        public ArcherCommand(PlayerIdentity            waitedInputPlayer, CardIdentity         archerCard,
                             IEnumerable<CardIdentity> availableCards,    Action<CardIdentity> callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            ArcherCard        = archerCard;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public CardIdentity ArcherCard { get; }

        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(CardIdentity result)
        {
            _callback(result);
        }

        public ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}