﻿using System.Collections.Generic;
using EnsureThat;

namespace OriflammeEngine.PlayerCards
{
    internal sealed class PlayerCardsSolver : IPlayerCardsSolver
    {
        private readonly IDictionary<CardIdentity, PlayerIdentity> _cards =
            new Dictionary<CardIdentity, PlayerIdentity>();

        public PlayerCardsSolver(IEnumerable<PlayerCard> cards)
        {
            foreach (var (cardIdentity, player) in cards)
            {
                _cards.Add(cardIdentity, player);
            }
        }

        public PlayerIdentity Get(CardIdentity cardIdentity)
        {
            Ensure.That(_cards).ContainsKey(cardIdentity);

            return _cards[cardIdentity];
        }
    }
}