﻿using System.Collections.Generic;
using EnsureThat;

namespace OriflammeEngine.Hand
{
    internal sealed class HandSolver : IHandSolver
    {
        private readonly IDictionary<PlayerIdentity, ISet<CardIdentity>> _hands =
            new Dictionary<PlayerIdentity, ISet<CardIdentity>>();

        public HandSolver(IEnumerable<Hand> hands)
        {
            foreach (var (player, cards) in hands)
            {
                _hands.Add(player, cards);
            }
        }

        public IEnumerable<CardIdentity> Get(PlayerIdentity player)
        {
            Ensure.That(_hands.ContainsKey(player)).IsTrue();

            return _hands[player];
        }

        public void Remove(PlayerIdentity player, CardIdentity cardIdentity)
        {
            Ensure.That(_hands.ContainsKey(player)).IsTrue();

            _hands[player].Remove(cardIdentity);
        }
    }
}