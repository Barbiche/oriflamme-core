﻿using System;
using System.Linq;
using OriflammeCore.Commands;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public sealed class Assassination : IRole
    {
        private readonly ICommandBus            _commandBus;
        private readonly IGameStack             _gameStack;
        private readonly IKillBus               _killBus;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly CardIdentity           _roleCard;
        private readonly IStateBus              _stateBus;

        public Assassination(CardIdentity           roleCard,
                             IGameStack             gameStack,
                             IPlayerInfluenceSolver playerInfluenceSolver,
                             IStateBus              stateBus,
                             ICommandBus            commandBus,
                             IKillBus               killBus,
                             IPlayerCardsSolver     playerCardsSolver)
        {
            _roleCard              = roleCard;
            _gameStack             = gameStack;
            _playerInfluenceSolver = playerInfluenceSolver;
            _stateBus              = stateBus;
            _commandBus            = commandBus;
            _killBus               = killBus;
            _playerCardsSolver     = playerCardsSolver;
        }

        public void Execute()
        {
            var killableCards = _gameStack.GetCardsExcepted(_roleCard).ToHashSet();
            if (killableCards.Any())
            {
                _commandBus.Publish(
                    new AssassinationCommand(_playerCardsSolver.Get(_roleCard), _roleCard, killableCards, Kill));
            }
            else
            {
                throw new InvalidOperationException("Assassination: stack was empty but the role still executes.");
            }
        }

        private void Kill(CardIdentity cardToKill)
        {
            _stateBus.Publish(new AssassinationState(_roleCard, cardToKill));

            _killBus.RequestKill(_roleCard, cardToKill);

            _playerInfluenceSolver.Give(_playerCardsSolver.Get(_roleCard), 1);

            _gameStack.RemoveCard(_roleCard);
        }
    }
}