﻿using BarbFoundation;
using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class SoldierState : IState
    {
        public SoldierState(CardIdentity soldierCard, Option<CardIdentity> killedCard)
        {
            SoldierCard = soldierCard;
            KilledCard  = killedCard;
        }

        public CardIdentity         SoldierCard { get; }
        public Option<CardIdentity> KilledCard  { get; }
    }
}