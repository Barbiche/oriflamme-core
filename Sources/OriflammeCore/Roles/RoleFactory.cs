﻿using System;
using OriflammeCore.Commands;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Reveal;
using OriflammeEngine.Role;
using OriflammeEngine.Stack;

namespace OriflammeCore.Roles
{
    public class RoleFactory : IRoleFactory
    {
        private readonly ICommandBus            _commandBus;
        private readonly IGameStack             _gameStack;
        private readonly IInvestSolver          _investSolver;
        private readonly IKillBus               _killBus;
        private readonly IPlayerCardsSolver     _playerCardsSolver;
        private readonly IPlayerInfluenceSolver _playerInfluenceSolver;
        private readonly IRevealSolver          _revealSolver;
        private readonly IRoleSolver            _roleSolver;
        private readonly IStateBus              _stateBus;

        public RoleFactory(IPlayerInfluenceSolver playerInfluenceSolver,
                           IGameStack             gameStack,
                           IStateBus              stateBus,
                           ICommandBus            commandBus,
                           IKillBus               killBus,
                           IInvestSolver          investSolver,
                           IRevealSolver          revealSolver,
                           IRoleSolver            roleSolver,
                           IPlayerCardsSolver     playerCardsSolver)
        {
            _playerInfluenceSolver = playerInfluenceSolver;
            _gameStack             = gameStack;
            _stateBus              = stateBus;
            _commandBus            = commandBus;
            _killBus               = killBus;
            _investSolver          = investSolver;
            _revealSolver          = revealSolver;
            _roleSolver            = roleSolver;
            _playerCardsSolver     = playerCardsSolver;
        }

        public IRole Create(CardIdentity card)
        {
            return Create(_roleSolver.Get(card), card);
        }

        public IRole Create(Role role, CardIdentity card)
        {
            return role switch
            {
                Role.Soldier => new Soldier(card, _gameStack, _playerInfluenceSolver, _stateBus, _commandBus, _killBus,
                                            _playerCardsSolver),
                Role.Archer => new Archer(card, _gameStack, _playerInfluenceSolver, _stateBus, _commandBus,
                                          _killBus, _playerCardsSolver),
                Role.Spy => new Spy(card, _playerInfluenceSolver, _gameStack, _commandBus, _stateBus,
                                    _playerCardsSolver),
                Role.Lord => new Lord(card, _playerInfluenceSolver, _stateBus, _gameStack, _playerCardsSolver),
                Role.Assassination => new Assassination(card, _gameStack, _playerInfluenceSolver, _stateBus,
                                                        _commandBus,
                                                        _killBus, _playerCardsSolver),
                Role.RoyalDecree => new RoyalDecree(card, _gameStack, _commandBus, _stateBus, _playerCardsSolver),
                Role.Ambush => new Ambush(card, _gameStack, _playerInfluenceSolver, _stateBus,
                                          _investSolver, _playerCardsSolver),
                Role.Conspiracy => new Conspiracy(card, _gameStack, _playerInfluenceSolver, _stateBus, _investSolver,
                                                  _playerCardsSolver),
                Role.Heir => new Heir(card, _playerInfluenceSolver, _gameStack, _stateBus, _playerCardsSolver,
                                      _roleSolver),
                Role.Shapeshifter => new Shapeshifter(card, _playerInfluenceSolver, _stateBus, _commandBus, _gameStack,
                                                      this, _revealSolver, _playerCardsSolver, _roleSolver),
                _ => throw new ArgumentOutOfRangeException(nameof(role), role, null)
            };
        }
    }
}