﻿namespace OriflammeCore.States
{
    public interface IStateSender
    {
        void SendState(IState state);
    }
}