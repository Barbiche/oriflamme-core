﻿namespace OriflammeCore.Game
{
    public interface IGameFactory
    {
        IGame CreateGame(int numberOfPlayers);
    }
}