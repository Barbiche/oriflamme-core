﻿using System;
using System.Collections.Generic;
using EnsureThat;

namespace OriflammeEngine.Reveal
{
    internal sealed class RevealSolver : IRevealSolver
    {
        private readonly IDictionary<CardIdentity, VisibilityState> _cards =
            new Dictionary<CardIdentity, VisibilityState>();

        public RevealSolver(IEnumerable<CardIdentity> cards)
        {
            foreach (var cardIdentity in cards)
            {
                _cards.Add(cardIdentity, VisibilityState.Hidden);
            }
        }

        public VisibilityState Get(CardIdentity card)
        {
            Ensure.That(_cards).ContainsKey(card);
            return _cards[card];
        }

        public VisibilityState Flip(CardIdentity card)
        {
            Ensure.That(_cards).ContainsKey(card);
            var value = _cards[card];
            switch (value)
            {
                case VisibilityState.Revealed:
                    _cards[card] = VisibilityState.Hidden;
                    return _cards[card];
                case VisibilityState.Hidden:
                    _cards[card] = VisibilityState.Revealed;
                    return _cards[card];
                default:
                    throw new ApplicationException("State inconsistency.");
            }
        }
    }
}