﻿using System.Collections.Generic;

namespace OriflammeEngine.Crown
{
    public sealed class CrownSolverFactory : ICrownSolverFactory
    {
        public ICrownSolver Create(IEnumerable<PlayerIdentity> players)
        {
            return new CrownSolver(players);
        }
    }
}