﻿using System;
using Equ;

namespace OriflammeEngine
{
    /// <summary>
    ///     Represents the identity of a player.
    /// </summary>
    public sealed class PlayerIdentity : MemberwiseEquatable<PlayerIdentity>
    {
        private readonly Guid _guid = Guid.NewGuid();

        public static implicit operator Guid(PlayerIdentity id)
        {
            return id._guid;
        }
    }
}