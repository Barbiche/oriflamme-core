﻿using System;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    /// <summary>
    ///     A request from the core to player of the reveal of a specific card.
    /// </summary>
    public class ChooseToRevealCard : IBooleanCommand
    {
        private readonly Action<bool> _callback;

        public ChooseToRevealCard(PlayerIdentity waitedInputPlayer, CardIdentity concernedCard, Action<bool> callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            Card              = concernedCard;
        }

        public CardIdentity   Card              { get; }
        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(bool result)
        {
            _callback.Invoke(result);
        }
    }
}