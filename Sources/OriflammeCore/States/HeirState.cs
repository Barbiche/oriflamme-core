﻿using BarbFoundation;
using OriflammeEngine;

namespace OriflammeCore.States
{
    public class HeirState : IState
    {
        public HeirState(Option<PlayerIdentity> playerWon)
        {
            PlayerWon = playerWon;
        }

        public Option<PlayerIdentity> PlayerWon { get; }
    }
}