﻿using OriflammeEngine;

namespace OriflammeCore.Roles
{
    public interface IKillBus : IKillExposer
    {
        /// <summary>
        ///     Request to kill a card, apply onKill behavior if needed.
        /// </summary>
        /// <param name="killerCard">The card which kills.</param>
        /// <param name="cardToKill">The card being killed.</param>
        /// <returns>True if the cardToKill is dead, false if not.</returns>
        void RequestKill(CardIdentity killerCard, CardIdentity cardToKill);
    }
}