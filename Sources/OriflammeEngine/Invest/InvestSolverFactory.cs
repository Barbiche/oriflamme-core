﻿using System.Collections.Generic;

namespace OriflammeEngine.Invest
{
    public class InvestSolverFactory : IInvestSolverFactory
    {
        public IInvestSolver Create(IEnumerable<CardIdentity> cards)
        {
            return new InvestSolver(cards);
        }
    }
}