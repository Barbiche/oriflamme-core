﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using OriflammeEngine;

namespace OriflammeCore.Roles
{
    public class KillBus : IKillBus
    {
        private readonly ISubject<KillRequest> _stateSubject = new Subject<KillRequest>();

        public IObservable<KillRequest> KillStream => _stateSubject.AsObservable();

        public void RequestKill(CardIdentity killerCard, CardIdentity cardToKill)
        {
            _stateSubject.OnNext(new KillRequest(killerCard, cardToKill));
        }
    }
}