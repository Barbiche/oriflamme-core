﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class ShapeshifterState : IState
    {
        public ShapeshifterState(CardIdentity shapeShifterCard, CardIdentity copiedCard)
        {
            ShapeShifterCard = shapeShifterCard;
            CopiedCard       = copiedCard;
        }

        public CardIdentity ShapeShifterCard { get; }
        public CardIdentity CopiedCard       { get; }
    }
}