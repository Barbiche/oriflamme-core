﻿using OriflammeEngine.PlayerCards;
using OriflammeEngine.Reveal;

namespace OriflammeEngine.Stack
{
    public sealed class GameStackFactory : IGameStackFactory
    {
        public IGameStack Create(IRevealSolver revealSolver, IPlayerCardsSolver playerCardsSolver)
        {
            return new GameStack(revealSolver, playerCardsSolver);
        }
    }
}