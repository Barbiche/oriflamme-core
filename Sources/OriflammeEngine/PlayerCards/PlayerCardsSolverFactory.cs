﻿using System.Collections.Generic;

namespace OriflammeEngine.PlayerCards
{
    public sealed class PlayerCardsSolverFactory : IPlayerCardsSolverFactory
    {
        public IPlayerCardsSolver Create(IEnumerable<PlayerCard> playerCards)
        {
            return new PlayerCardsSolver(playerCards);
        }
    }
}