﻿using BarbFoundation;
using OriflammeEngine;
using OriflammeEngine.Hand;
using OriflammeEngine.Stack;

namespace OriflammeCore.Game
{
    public interface IGameExposer
    {
        IGameStack           GameStack  { get; }
        IHandSolver          HandSolver { get; }
        Option<CardIdentity> Current    { get; }
    }
}