﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public sealed class ShapeshifterCommand : ICardBetweenManyCommand
    {
        private readonly Action<CardIdentity> _callback;

        public ShapeshifterCommand(PlayerIdentity            waitedInputPlayer,
                                   CardIdentity              shapeshifterCard,
                                   IEnumerable<CardIdentity> availableCards,
                                   Action<CardIdentity>      callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            ShapeshifterCard  = shapeshifterCard;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public CardIdentity ShapeshifterCard { get; }

        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(CardIdentity result)
        {
            _callback(result);
        }

        public ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}