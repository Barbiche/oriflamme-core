﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public readonly struct AmbushKilled : IState
    {
        public CardIdentity   AmbushCard   { get; init; }
        public PlayerIdentity AmbushPlayer { get; init; }
        public CardIdentity   KillerCard   { get; init; }
    }
}