﻿using System.Collections.Generic;
using BarbFoundation;
using BarbFoundation.DataStructures;

namespace OriflammeEngine.Stack
{
    /// <summary>
    ///     Represents a game stack for a game.
    /// </summary>
    public interface IGameStack
    {
        /// <summary>
        ///     Returns all cards present in the stack.
        /// </summary>
        IEnumerable<CardIdentity> Cards { get; }

        /// <summary>
        ///     Returns all revealed cards present in the stack.
        /// </summary>
        IEnumerable<CardIdentity> RevealedCards { get; }

        /// <summary>
        ///     Internal stack.
        /// </summary>
        IReadOnlyLinkedList<CardIdentity> InternalStack { get; }

        /// <summary>
        ///     Get the next card in the stack.
        /// </summary>
        /// <param name="card">Card to find the next.</param>
        /// <returns>Next card.</returns>
        Option<CardIdentity> GetNext(CardIdentity card);

        /// <summary>
        ///     Get the previous card in the stack.
        /// </summary>
        /// <param name="card">Card to find the previous.</param>
        /// <returns>Previous card.</returns>
        Option<CardIdentity> GetPrevious(CardIdentity card);

        /// <summary>
        ///     Add a card at the start of the stack.
        /// </summary>
        /// <param name="card">Card to add in the stack.</param>
        void AddCardAtStart(CardIdentity card);

        /// <summary>
        ///     Add a card at the end of the stack.
        /// </summary>
        /// <param name="card">Card to add in the stack.</param>
        void AddCardAtEnd(CardIdentity card);

        /// <summary>
        ///     Remove a card in the stack.
        /// </summary>
        /// <param name="card">Card to remove from the stack.</param>
        void RemoveCard(CardIdentity card);

        /// <summary>
        ///     Gets the first card.
        /// </summary>
        /// <returns>The first card of the stack.</returns>
        Option<CardIdentity> GetFirst();

        /// <summary>
        ///     Gets the last card.
        /// </summary>
        /// <returns>The last card of the stack.</returns>
        Option<CardIdentity> GetLast();

        /// <summary>
        ///     Move a card from the stack to be placed after another card.
        /// </summary>
        /// <param name="card">Card to move.</param>
        /// <param name="pivotCard">After the move operation, the card will be placed after the pivot card.</param>
        void MoveAfter(CardIdentity card, CardIdentity pivotCard);

        /// <summary>
        ///     Move a card from the stack to be placed at the beginning.
        /// </summary>
        /// <param name="card">Card to move.</param>
        void MoveAtStart(CardIdentity card);

        /// <summary>
        ///     Returns all cards in the stack of a specific player.
        /// </summary>
        /// <param name="player">Player having the wanted cards.</param>
        /// <returns>Cards of the given player.</returns>
        IEnumerable<CardIdentity> GetPlayerCards(PlayerIdentity player);

        /// <summary>
        ///     Returns all cards in the stack of a specific player excepted one.
        /// </summary>
        /// <param name="player">Player having the wanted cards.</param>
        /// <param name="excepted">A card which should not be returned.</param>
        /// <returns>Cards of the given player.</returns>
        IEnumerable<CardIdentity> GetPlayerCardsExcepted(PlayerIdentity player, CardIdentity excepted);

        /// <summary>
        ///     Returns all cards in the stack excepted one.
        /// </summary>
        /// <param name="excepted">A card which should not be returned.</param>
        /// <returns>Cards.</returns>
        IEnumerable<CardIdentity> GetCardsExcepted(CardIdentity excepted);
    }
}