﻿namespace OriflammeEngine.Role
{
    public enum Role
    {
        Soldier,
        Archer,
        Spy,
        Lord,
        Assassination,
        RoyalDecree,
        Ambush,
        Conspiracy,
        Heir,
        Shapeshifter
    }
}