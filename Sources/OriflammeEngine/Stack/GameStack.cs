﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using BarbFoundation;
using BarbFoundation.DataStructures;
using EnsureThat;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.Reveal;

namespace OriflammeEngine.Stack
{
    internal sealed class GameStack : IGameStack, ICardRemovedExposer
    {
        private readonly IPlayerCardsSolver       _playerCardsSolver;
        private readonly ISubject<CardIdentity>   _removedCardSubject = new Subject<CardIdentity>();
        private readonly IRevealSolver            _revealSolver;
        private readonly LinkedList<CardIdentity> _stack = new();

        public GameStack(IRevealSolver revealSolver, IPlayerCardsSolver playerCardsSolver)
        {
            _revealSolver      = revealSolver;
            _playerCardsSolver = playerCardsSolver;
            InternalStack      = new ReadOnlyLinkedList<CardIdentity>(_stack);
        }

        public IObservable<CardIdentity> RemovedCardUpdates => _removedCardSubject.AsObservable();

        /// <inheritdoc />
        public Option<CardIdentity> GetNext(CardIdentity card)
        {
            var next = _stack.Find(card).Next;
            return next == null ? OptionExtensions.None<CardIdentity>() : OptionExtensions.Some(next.Value);
        }

        /// <inheritdoc />
        public Option<CardIdentity> GetPrevious(CardIdentity card)
        {
            var previous = _stack.Find(card).Previous;
            return previous == null ? OptionExtensions.None<CardIdentity>() : OptionExtensions.Some(previous.Value);
        }

        /// <inheritdoc />
        public IEnumerable<CardIdentity> Cards => _stack;

        /// <inheritdoc />
        public IEnumerable<CardIdentity> RevealedCards =>
            _stack.Where(x => _revealSolver.Get(x) is VisibilityState.Revealed);

        /// <inheritdoc />
        public void AddCardAtStart(CardIdentity card)
        {
            _stack.AddFirst(card);
        }

        /// <inheritdoc />
        public void AddCardAtEnd(CardIdentity card)
        {
            _stack.AddLast(card);
        }

        /// <inheritdoc />
        public void RemoveCard(CardIdentity card)
        {
            _removedCardSubject.OnNext(card);
            _stack.Remove(card);
        }

        /// <inheritdoc />
        public Option<CardIdentity> GetFirst()
        {
            var first = _stack.First;
            return first == null ? OptionExtensions.None<CardIdentity>() : OptionExtensions.Some(first.Value);
        }

        /// <inheritdoc />
        public Option<CardIdentity> GetLast()
        {
            var last = _stack.Last;
            return last == null ? OptionExtensions.None<CardIdentity>() : OptionExtensions.Some(last.Value);
        }

        public void MoveAfter(CardIdentity card, CardIdentity pivotCard)
        {
            EnsureArg.IsTrue(card != pivotCard, nameof(card),
                             options => options.WithException(
                                 new InvalidOperationException("Can't move a card after the same.")));

            RemoveCard(card);
            AddAfter(card, pivotCard);
        }

        public void MoveAtStart(CardIdentity card)
        {
            RemoveCard(card);
            AddCardAtStart(card);
        }

        /// <inheritdoc />
        public IReadOnlyLinkedList<CardIdentity> InternalStack { get; }

        /// <inheritdoc />
        public IEnumerable<CardIdentity> GetPlayerCards(PlayerIdentity player)
        {
            return Cards.Where(c => _playerCardsSolver.Get(c) == player);
        }

        /// <inheritdoc />
        public IEnumerable<CardIdentity> GetPlayerCardsExcepted(PlayerIdentity player, CardIdentity excepted)
        {
            return Cards.Where(c => _playerCardsSolver.Get(c) == player && c != excepted);
        }

        /// <inheritdoc />
        public IEnumerable<CardIdentity> GetCardsExcepted(CardIdentity excepted)
        {
            return Cards.Where(c => c != excepted);
        }

        private void AddAfter(CardIdentity card, CardIdentity pivotCard)
        {
            _stack.AddAfter(_stack.Find(pivotCard) ?? throw new InvalidOperationException(), card);
        }
    }
}