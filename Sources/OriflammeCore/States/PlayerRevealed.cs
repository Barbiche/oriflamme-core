﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public class PlayerRevealed : IState
    {
        public PlayerRevealed(PlayerIdentity player, int influenceGain, CardIdentity revealedCard)
        {
            Player        = player;
            InfluenceGain = influenceGain;
            RevealedCard  = revealedCard;
        }

        public PlayerIdentity Player        { get; }
        public int            InfluenceGain { get; }
        public CardIdentity   RevealedCard  { get; }
    }
}