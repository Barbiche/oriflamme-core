﻿using System.Collections.Generic;
using System.Linq;
using OriflammeCore.Commands;
using OriflammeCore.Roles;
using OriflammeCore.States;
using OriflammeEngine;
using OriflammeEngine.Crown;
using OriflammeEngine.Hand;
using OriflammeEngine.Invest;
using OriflammeEngine.PlayerCards;
using OriflammeEngine.PlayerInfluence;
using OriflammeEngine.Reveal;
using OriflammeEngine.Role;
using OriflammeEngine.Stack;

namespace OriflammeCore.Game
{
    public sealed class GameFactory : IGameFactory
    {
        private readonly ICrownSolverFactory           _crownSolverFactory;
        private readonly IGameStackFactory             _gameStackFactory;
        private readonly IHandSolverFactory            _handSolverFactory;
        private readonly IInvestSolverFactory          _investSolverFactory;
        private readonly IPlayerCardsSolverFactory     _playerCardsSolverFactory;
        private readonly IPlayerInfluenceSolverFactory _playerInfluenceSolverFactory;
        private readonly IRevealSolverFactory          _revealSolverFactory;
        private readonly IRoleSolverFactory            _roleSolverFactory;

        public GameFactory(IPlayerInfluenceSolverFactory playerInfluenceSolverFactory,
                           IHandSolverFactory            handSolverFactory,
                           IInvestSolverFactory          investSolverFactory,
                           IRevealSolverFactory          revealSolverFactory,
                           ICrownSolverFactory           crownSolverFactory,
                           IPlayerCardsSolverFactory     playerCardsSolverFactory,
                           IGameStackFactory             gameStackFactory,
                           IRoleSolverFactory            roleSolverFactory)
        {
            _playerInfluenceSolverFactory = playerInfluenceSolverFactory;
            _handSolverFactory            = handSolverFactory;
            _investSolverFactory          = investSolverFactory;
            _revealSolverFactory          = revealSolverFactory;
            _crownSolverFactory           = crownSolverFactory;
            _playerCardsSolverFactory     = playerCardsSolverFactory;
            _gameStackFactory             = gameStackFactory;
            _roleSolverFactory            = roleSolverFactory;
        }

        public IGame CreateGame(int numberOfPlayers)
        {
            // Players
            var players = CreatePlayers(numberOfPlayers);

            // Cards and hands
            var allCards = new HashSet<CardData>();
            foreach (var player in players)
            {
                foreach (var cardData in CreateStartingHand(player))
                {
                    allCards.Add(cardData);
                }
            }

            var investSolver = _investSolverFactory.Create(allCards.Select(cd => cd.Card));
            var revealSolver = _revealSolverFactory.Create(allCards.Select(cd => cd.Card));

            var handSolver = _handSolverFactory.Create(allCards.GroupBy(cd => cd.PlayerIdentity)
                                                           .Select(group => new Hand(
                                                                       group.Key,
                                                                       group.Select(c => c.Card).ToHashSet())));
            var playerCardsSolver =
                _playerCardsSolverFactory.Create(allCards.Select(cd => new PlayerCard(cd.Card, cd.PlayerIdentity)));
            var scoreSolver = _playerInfluenceSolverFactory.Create(players);
            var crownSolver = _crownSolverFactory.Create(players);
            var gameStack   = _gameStackFactory.Create(revealSolver, playerCardsSolver);
            var roleSolver  = _roleSolverFactory.Create(allCards.Select(cd => new RoleCard(cd.Card, cd.Role)));

            // Utils
            var stateBus   = new StateBus();
            var commandBus = new CommandBus();
            var killBus    = new KillBus();

            // Roles
            var roleFactory = new RoleFactory(scoreSolver, gameStack, stateBus, commandBus, killBus,
                                              investSolver, revealSolver, roleSolver, playerCardsSolver);
            var roleExecutor = new RoleExecutor(roleFactory, scoreSolver, stateBus, killBus, gameStack,
                                                investSolver, revealSolver, playerCardsSolver);

            return new Game(players,
                            gameStack,
                            stateBus,
                            commandBus,
                            scoreSolver,
                            handSolver,
                            roleExecutor,
                            crownSolver,
                            new GameStackPointerFactory(gameStack, (ICardRemovedExposer)gameStack),
                            investSolver,
                            revealSolver,
                            playerCardsSolver);
        }

        private static CardData[] CreateStartingHand(PlayerIdentity player)
        {
            return new[]
            {
                new CardData(new CardIdentity(), player, Role.Ambush),
                new CardData(new CardIdentity(), player, Role.Archer),
                new CardData(new CardIdentity(), player, Role.Assassination),
                new CardData(new CardIdentity(), player, Role.Conspiracy),
                new CardData(new CardIdentity(), player, Role.Heir),
                new CardData(new CardIdentity(), player, Role.Lord),
                new CardData(new CardIdentity(), player, Role.RoyalDecree),
                new CardData(new CardIdentity(), player, Role.Shapeshifter),
                new CardData(new CardIdentity(), player, Role.Spy),
                new CardData(new CardIdentity(), player, Role.Soldier),
            };
        }

        private static PlayerIdentity[] CreatePlayers(int numberOfPlayers)
        {
            var players = new PlayerIdentity[numberOfPlayers];
            for (var i = 0; i < numberOfPlayers; i++)
            {
                players[i] = new PlayerIdentity();
            }

            return players;
        }

        private record CardData(CardIdentity Card, PlayerIdentity PlayerIdentity, Role Role);
    }
}