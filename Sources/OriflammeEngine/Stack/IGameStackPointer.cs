﻿using BarbFoundation;

namespace OriflammeEngine.Stack
{
    /// <summary>
    ///     Represents pointing capabilities for this game stack.
    /// </summary>
    public interface IGameStackPointer
    {
        /// <summary>
        ///     Currently pointed card. If None, then no card is currently pointed.
        /// </summary>
        Option<CardIdentity> PointedCard { get; }

        /// <summary>
        ///     Moves the pointer forward.
        ///     If no card was pointed, start by the first card.
        /// </summary>
        /// <returns>
        ///     Identity of the newly pointed card. If the end of the stack was reached, then returns None.
        /// </returns>
        public Option<CardIdentity> MoveForward();

        /// <summary>
        ///     Moves the pointer backward. If no card was pointed, start by the last card.
        /// </summary>
        /// <returns>
        ///     Identity of the newly pointed card. If the beginning of the stack was reached, then returns None.
        /// </returns>
        public Option<CardIdentity> MoveBackward();
    }
}