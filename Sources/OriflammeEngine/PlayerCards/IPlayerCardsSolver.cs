﻿namespace OriflammeEngine.PlayerCards
{
    /// <summary>
    ///     Represents a solver to associate cards to player.
    /// </summary>
    public interface IPlayerCardsSolver
    {
        /// <summary>
        ///     Gets the player associated to the card.
        /// </summary>
        /// <param name="cardIdentity">Card to get the player from.</param>
        /// <returns>Player associated to the card.</returns>
        PlayerIdentity Get(CardIdentity cardIdentity);
    }
}