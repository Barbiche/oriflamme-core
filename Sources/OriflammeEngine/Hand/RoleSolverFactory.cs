﻿using System.Collections.Generic;

namespace OriflammeEngine.Hand
{
    public class HandSolverFactory : IHandSolverFactory
    {
        public IHandSolver Create(IEnumerable<Hand> hands)
        {
            return new HandSolver(hands);
        }
    }
}