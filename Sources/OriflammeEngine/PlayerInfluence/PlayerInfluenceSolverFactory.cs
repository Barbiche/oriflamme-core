﻿using System.Collections.Generic;

namespace OriflammeEngine.PlayerInfluence
{
    public class PlayerInfluenceSolverFactory : IPlayerInfluenceSolverFactory
    {
        public IPlayerInfluenceSolver Create(IEnumerable<PlayerIdentity> players)
        {
            return new PlayerInfluenceSolver(players);
        }
    }
}