﻿using OriflammeCore.Commands;
using OriflammeCore.States;

namespace OriflammeCore.Game
{
    public interface IGame : IStateExposer, ICommandExposer, IGameExposer
    {
        void StartGame();
    }
}