﻿namespace OriflammeCore.Roles
{
    public interface IReveal
    {
        void Reveal();
    }
}