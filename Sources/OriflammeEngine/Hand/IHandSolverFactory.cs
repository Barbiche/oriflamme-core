﻿using System.Collections.Generic;

namespace OriflammeEngine.Hand
{
    public interface IHandSolverFactory
    {
        IHandSolver Create(IEnumerable<Hand> hands);
    }

    public record Hand(PlayerIdentity PlayerIdentity, ISet<CardIdentity> Cards);
}