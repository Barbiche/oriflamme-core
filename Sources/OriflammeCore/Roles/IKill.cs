﻿using OriflammeEngine;

namespace OriflammeCore.Roles
{
    public interface IKill
    {
        bool OnKilled(CardIdentity killerCard);
    }
}