﻿using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public interface IPlayerBetweenManyCommand : ICommand<PlayerIdentity>
    {
        ImmutableArray<PlayerIdentity> AvailablePlayers { get; }
    }
}