﻿namespace OriflammeCore.Commands
{
    /// <summary>
    ///     Represents a bus for Commands.
    /// </summary>
    public interface ICommandBus : ICommandExposer
    {
        /// <summary>
        ///     Publish a command in the bus, and calls an action when the result is received.
        /// </summary>
        /// <param name="command">Command to publish.</param>
        void Publish(ICommand command);
    }
}