﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public sealed class AssassinationCommand : ICardBetweenManyCommand
    {
        private readonly Action<CardIdentity> _callback;

        public AssassinationCommand(PlayerIdentity            waitedInputPlayer,
                                    CardIdentity              assassinationCard,
                                    IEnumerable<CardIdentity> availableCards,
                                    Action<CardIdentity>      callback)
        {
            WaitedInputPlayer = waitedInputPlayer;
            AssassinationCard = assassinationCard;
            _callback         = callback;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public CardIdentity AssassinationCard { get; }

        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(CardIdentity result)
        {
            _callback(result);
        }

        public ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}