﻿using OriflammeEngine;

namespace OriflammeCore.States
{
    public sealed class RoyalDecreeState : IState
    {
        public RoyalDecreeState(PlayerIdentity royalDecreePlayer, CardIdentity movedCard)
        {
            RoyalDecreePlayer = royalDecreePlayer;
            MovedCard         = movedCard;
        }

        public PlayerIdentity RoyalDecreePlayer { get; }
        public CardIdentity   MovedCard         { get; }
    }
}