﻿namespace OriflammeEngine.Reveal
{
    /// <summary>
    ///     Represents a solver for reveal behaviors.
    /// </summary>
    public interface IRevealSolver
    {
        /// <summary>
        ///     Returns the state of a card.
        /// </summary>
        /// <param name="card">Card to check.</param>
        /// <returns>Visibility state of the card.</returns>
        VisibilityState Get(CardIdentity card);

        /// <summary>
        ///     Flip the card.
        /// </summary>
        /// <param name="card">Card to flip.</param>
        /// <returns>New visibility state of the card.</returns>
        VisibilityState Flip(CardIdentity card);
    }
}