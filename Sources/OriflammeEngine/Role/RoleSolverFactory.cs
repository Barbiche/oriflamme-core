﻿using System.Collections.Generic;

namespace OriflammeEngine.Role
{
    public class RoleSolverFactory : IRoleSolverFactory
    {
        public IRoleSolver Create(IEnumerable<RoleCard> cards)
        {
            return new RoleSolver(cards);
        }
    }
}