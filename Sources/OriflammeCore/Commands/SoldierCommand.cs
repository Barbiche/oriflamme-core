﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using OriflammeEngine;

namespace OriflammeCore.Commands
{
    public sealed class SoldierCommand : ICardBetweenManyCommand
    {
        private readonly Action<CardIdentity> _callback;

        public SoldierCommand(PlayerIdentity            waitedInputPlayer, CardIdentity         soldierCard,
                              IEnumerable<CardIdentity> availableCards,    Action<CardIdentity> callback)
        {
            _callback         = callback;
            WaitedInputPlayer = waitedInputPlayer;
            SoldierCard       = soldierCard;
            AvailableCards    = availableCards.ToImmutableArray();
        }

        public CardIdentity SoldierCard { get; }

        public PlayerIdentity WaitedInputPlayer { get; }

        public void SendResult(CardIdentity result)
        {
            _callback(result);
        }

        public ImmutableArray<CardIdentity> AvailableCards { get; }
    }
}