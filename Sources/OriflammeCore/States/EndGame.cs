﻿using System.Collections.Generic;
using System.Linq;
using OriflammeEngine;

namespace OriflammeCore.States
{
    public class EndGame : IState
    {
        public EndGame(IEnumerable<(PlayerIdentity player, int totalInfluence)> scores)
        {
            Scores = scores.OrderByDescending(p => p.totalInfluence);
        }

        public IEnumerable<(PlayerIdentity player, int totalInfluence)> Scores { get; }
        public PlayerIdentity Winner => Scores.OrderByDescending(p => p.totalInfluence).Select(p => p.player).First();
    }
}